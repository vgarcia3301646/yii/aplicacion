<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "alumnos".
 *
 * @property int $id_alumno
 * @property string|null $dni
 * @property string|null $nombre
 * @property string|null $apellidos
 * @property string|null $curso
 * @property string|null $aula
 * @property string|null $turno
 * @property string|null $estado_matricula
 * @property int|null $id_portatil
 * @property int|null $id_cargador
 * @property int|null $id_raton
 *
 * @property Cargadores $cargador
 * @property Portatiles $portatil
 * @property Ratones $raton
 */
class Alumnos extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'alumnos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_portatil', 'id_cargador', 'id_raton'], 'integer'],
            [['dni', 'turno'], 'string', 'max' => 8],
            [['nombre'], 'string', 'max' => 24],
            [['apellidos'], 'string', 'max' => 48],
            [['curso'], 'string', 'max' => 96],
            [['aula'], 'string', 'max' => 4],
            [['estado_matricula'], 'string', 'max' => 16],
            [['dni'], 'unique'],
            [['id_cargador'], 'exist', 'skipOnError' => true, 'targetClass' => Cargadores::class, 'targetAttribute' => ['id_cargador' => 'id_cargador']],
            [['id_portatil'], 'exist', 'skipOnError' => true, 'targetClass' => Portatiles::class, 'targetAttribute' => ['id_portatil' => 'id_portatil']],
            [['id_raton'], 'exist', 'skipOnError' => true, 'targetClass' => Ratones::class, 'targetAttribute' => ['id_raton' => 'id_raton']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_alumno' => 'Id Alumno',
            'dni' => 'Dni',
            'nombre' => 'Nombre',
            'apellidos' => 'Apellidos',
            'curso' => 'Curso',
            'aula' => 'Aula',
            'turno' => 'Turno',
            'estado_matricula' => 'Estado Matricula',
            'id_portatil' => 'Id Portatil',
            'id_cargador' => 'Id Cargador',
            'id_raton' => 'Id Raton',
        ];
    }

    /**
     * Gets query for [[Cargador]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCargador()
    {
        return $this->hasOne(Cargadores::class, ['id_cargador' => 'id_cargador']);
    }

    /**
     * Gets query for [[Portatil]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPortatil()
    {
        return $this->hasOne(Portatiles::class, ['id_portatil' => 'id_portatil']);
    }

    /**
     * Gets query for [[Raton]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getRaton()
    {
        return $this->hasOne(Ratones::class, ['id_raton' => 'id_raton']);
    }
}
