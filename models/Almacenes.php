<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "almacenes".
 *
 * @property int $id_almacen
 * @property string|null $aula
 * @property int|null $capacidad
 *
 * @property Cargadores[] $cargadores
 * @property Portatiles[] $portatiles
 * @property Ratones[] $ratones
 */
class Almacenes extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'almacenes';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['capacidad'], 'integer'],
            [['aula'], 'string', 'max' => 4],
            [['aula'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_almacen' => 'Id Almacen',
            'aula' => 'Aula',
            'capacidad' => 'Capacidad',
        ];
    }

    /**
     * Gets query for [[Cargadores]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCargadores()
    {
        return $this->hasMany(Cargadores::class, ['id_almacen' => 'id_almacen']);
    }

    /**
     * Gets query for [[Portatiles]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPortatiles()
    {
        return $this->hasMany(Portatiles::class, ['id_almacen' => 'id_almacen']);
    }

    /**
     * Gets query for [[Ratones]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getRatones()
    {
        return $this->hasMany(Ratones::class, ['id_almacen' => 'id_almacen']);
    }
}
