<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "cargadores".
 *
 * @property int $id_cargador
 * @property string|null $codigo
 * @property int|null $potencia
 * @property string|null $estado_alquiler
 * @property int|null $id_almacen
 *
 * @property Almacenes $almacen
 * @property Alumnos[] $alumnos
 * @property Portatiles[] $portatiles
 */
class Cargadores extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'cargadores';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['potencia', 'id_almacen'], 'integer'],
            [['codigo'], 'string', 'max' => 4],
            [['estado_alquiler'], 'string', 'max' => 24],
            [['codigo'], 'unique'],
            [['id_almacen'], 'exist', 'skipOnError' => true, 'targetClass' => Almacenes::class, 'targetAttribute' => ['id_almacen' => 'id_almacen']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_cargador' => 'Id Cargador',
            'codigo' => 'Codigo',
            'potencia' => 'Potencia',
            'estado_alquiler' => 'Estado Alquiler',
            'id_almacen' => 'Id Almacen',
        ];
    }

    /**
     * Gets query for [[Almacen]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getAlmacen()
    {
        return $this->hasOne(Almacenes::class, ['id_almacen' => 'id_almacen']);
    }

    /**
     * Gets query for [[Alumnos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getAlumnos()
    {
        return $this->hasMany(Alumnos::class, ['id_cargador' => 'id_cargador']);
    }

    /**
     * Gets query for [[Portatiles]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPortatiles()
    {
        return $this->hasMany(Portatiles::class, ['id_cargador' => 'id_cargador']);
    }
}
