<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "portatiles".
 *
 * @property int $id_portatil
 * @property string|null $codigo
 * @property string|null $marca
 * @property string|null $modelo
 * @property string|null $procesador
 * @property int|null $memoria_ram
 * @property string|null $dispositivo_almacenamiento
 * @property int|null $capacidad_almacenamiento
 * @property string|null $estado_alquiler
 * @property int|null $id_cargador
 * @property int|null $id_raton
 * @property int|null $id_almacen
 *
 * @property Almacenes $almacen
 * @property Alumnos[] $alumnos
 * @property AplicacionesInstaladas[] $aplicacionesInstaladas
 * @property Cargadores $cargador
 * @property Ratones $raton
 */
class Portatiles extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'portatiles';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['memoria_ram', 'capacidad_almacenamiento', 'id_cargador', 'id_raton', 'id_almacen'], 'integer'],
            [['codigo'], 'string', 'max' => 4],
            [['marca', 'modelo', 'procesador', 'dispositivo_almacenamiento', 'estado_alquiler'], 'string', 'max' => 24],
            [['codigo'], 'unique'],
            [['id_almacen'], 'exist', 'skipOnError' => true, 'targetClass' => Almacenes::class, 'targetAttribute' => ['id_almacen' => 'id_almacen']],
            [['id_cargador'], 'exist', 'skipOnError' => true, 'targetClass' => Cargadores::class, 'targetAttribute' => ['id_cargador' => 'id_cargador']],
            [['id_raton'], 'exist', 'skipOnError' => true, 'targetClass' => Ratones::class, 'targetAttribute' => ['id_raton' => 'id_raton']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_portatil' => 'Id Portatil',
            'codigo' => 'Codigo',
            'marca' => 'Marca',
            'modelo' => 'Modelo',
            'procesador' => 'Procesador',
            'memoria_ram' => 'Memoria Ram',
            'dispositivo_almacenamiento' => 'Dispositivo Almacenamiento',
            'capacidad_almacenamiento' => 'Capacidad Almacenamiento',
            'estado_alquiler' => 'Estado Alquiler',
            'id_cargador' => 'Id Cargador',
            'id_raton' => 'Id Raton',
            'id_almacen' => 'Id Almacen',
        ];
    }

    /**
     * Gets query for [[Almacen]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getAlmacen()
    {
        return $this->hasOne(Almacenes::class, ['id_almacen' => 'id_almacen']);
    }

    /**
     * Gets query for [[Alumnos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getAlumnos()
    {
        return $this->hasMany(Alumnos::class, ['id_portatil' => 'id_portatil']);
    }

    /**
     * Gets query for [[AplicacionesInstaladas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getAplicacionesInstaladas()
    {
        return $this->hasMany(AplicacionesInstaladas::class, ['id_portatil' => 'id_portatil']);
    }

    /**
     * Gets query for [[Cargador]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCargador()
    {
        return $this->hasOne(Cargadores::class, ['id_cargador' => 'id_cargador']);
    }

    /**
     * Gets query for [[Raton]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getRaton()
    {
        return $this->hasOne(Ratones::class, ['id_raton' => 'id_raton']);
    }
}
