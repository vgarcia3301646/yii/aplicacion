<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "ratones".
 *
 * @property int $id_raton
 * @property string|null $codigo
 * @property string|null $marca
 * @property string|null $modelo
 * @property string|null $tipo_conector
 * @property string|null $estado_alquiler
 * @property int|null $id_almacen
 *
 * @property Almacenes $almacen
 * @property Alumnos[] $alumnos
 * @property Portatiles[] $portatiles
 */
class Ratones extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ratones';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_almacen'], 'integer'],
            [['codigo'], 'string', 'max' => 4],
            [['marca', 'modelo', 'estado_alquiler'], 'string', 'max' => 24],
            [['tipo_conector'], 'string', 'max' => 8],
            [['codigo'], 'unique'],
            [['id_almacen'], 'exist', 'skipOnError' => true, 'targetClass' => Almacenes::class, 'targetAttribute' => ['id_almacen' => 'id_almacen']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_raton' => 'Id Raton',
            'codigo' => 'Codigo',
            'marca' => 'Marca',
            'modelo' => 'Modelo',
            'tipo_conector' => 'Tipo Conector',
            'estado_alquiler' => 'Estado Alquiler',
            'id_almacen' => 'Id Almacen',
        ];
    }

    /**
     * Gets query for [[Almacen]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getAlmacen()
    {
        return $this->hasOne(Almacenes::class, ['id_almacen' => 'id_almacen']);
    }

    /**
     * Gets query for [[Alumnos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getAlumnos()
    {
        return $this->hasMany(Alumnos::class, ['id_raton' => 'id_raton']);
    }

    /**
     * Gets query for [[Portatiles]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPortatiles()
    {
        return $this->hasMany(Portatiles::class, ['id_raton' => 'id_raton']);
    }
}
