<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var yii\web\View $this */
/** @var app\models\portatiles $model */
/** @var yii\widgets\ActiveForm $form */
?>

<div class="portatiles-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'codigo')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'marca')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'modelo')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'procesador')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'memoria_ram')->textInput() ?>

    <?= $form->field($model, 'dispositivo_almacenamiento')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'capacidad_almacenamiento')->textInput() ?>

    <?= $form->field($model, 'estado_alquiler')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'id_cargador')->textInput() ?>

    <?= $form->field($model, 'id_raton')->textInput() ?>

    <?= $form->field($model, 'id_almacen')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
