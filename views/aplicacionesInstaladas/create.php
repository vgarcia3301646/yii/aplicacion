<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\aplicacionesinstaladas $model */

$this->title = 'Create Aplicacionesinstaladas';
$this->params['breadcrumbs'][] = ['label' => 'Aplicacionesinstaladas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="aplicacionesinstaladas-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
