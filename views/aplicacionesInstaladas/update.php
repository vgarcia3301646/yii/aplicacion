<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\aplicacionesinstaladas $model */

$this->title = 'Update Aplicacionesinstaladas: ' . $model->id_aplicacion;
$this->params['breadcrumbs'][] = ['label' => 'Aplicacionesinstaladas', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_aplicacion, 'url' => ['view', 'id_aplicacion' => $model->id_aplicacion]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="aplicacionesinstaladas-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
