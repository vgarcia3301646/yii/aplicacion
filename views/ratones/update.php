<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\ratones $model */

$this->title = 'Update Ratones: ' . $model->id_raton;
$this->params['breadcrumbs'][] = ['label' => 'Ratones', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_raton, 'url' => ['view', 'id_raton' => $model->id_raton]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="ratones-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
