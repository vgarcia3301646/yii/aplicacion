<?php

use app\models\ratones;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/** @var yii\web\View $this */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = 'Ratones';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ratones-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Ratones', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id_raton',
            'codigo',
            'marca',
            'modelo',
            'tipo_conector',
            //'estado_alquiler',
            //'id_almacen',
            [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, ratones $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'id_raton' => $model->id_raton]);
                 }
            ],
        ],
    ]); ?>


</div>
