<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\ratones $model */

$this->title = 'Create Ratones';
$this->params['breadcrumbs'][] = ['label' => 'Ratones', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ratones-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
