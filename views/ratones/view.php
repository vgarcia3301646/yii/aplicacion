<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/** @var yii\web\View $this */
/** @var app\models\ratones $model */

$this->title = $model->id_raton;
$this->params['breadcrumbs'][] = ['label' => 'Ratones', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="ratones-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id_raton' => $model->id_raton], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id_raton' => $model->id_raton], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id_raton',
            'codigo',
            'marca',
            'modelo',
            'tipo_conector',
            'estado_alquiler',
            'id_almacen',
        ],
    ]) ?>

</div>
