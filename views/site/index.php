<?php

    /** @var yii\web\View $this */
    use yii\helpers\Html;
    use yii\web\View;
    $this->title = 'Inicio';

?>

<div class="site-index">

    <div class="body-content">

        <div style="height: auto; width: 1110px;">
            <canvas id="grafico" style="height:100%;max-height:528px"></canvas>
        </div>

        <script src="https://cdn.jsdelivr.net/npm/chart.js"></script>

        <script>
            
            const ctx = document.getElementById('grafico');

            var dispositivos = ['Portátiles', 'Cargadores', 'Ratones', 'Dispositivos averiados'];
            var valores = [<?= $portatiles['alquilados'] + $portatiles['disponibles'] ?>, <?= $cargadores['alquilados'] + $cargadores['disponibles'] ?>, <?= $ratones['alquilados'] + $ratones['disponibles'] ?>, <?= $portatiles['averiados'] + $cargadores['averiados'] + $ratones['averiados'] ?>];
            var paleta = ['#4E5AE3', '#44B86B', '#E89622', '#E8685C']

            new Chart(ctx, {
                type: 'pie',
                data: {
                    labels: dispositivos,
                    datasets: [{
                        label: 'Total',
                        data: valores,
                        backgroundColor: paleta,
                        borderColor: '#000000',
                        borderWidth: 1,
                        radius: 200
                    }]
                },
                options: {
                    plugins: {
                        legend: {
                            position: 'chartArea',
                            labels: {
                                font: {
                                    family: 'Helvetica',
                                    size: 16
                                }
                            }
                        }
                    }
                }
            });
          
        </script>


        <div class="row">
            
            <div class="col-lg-4 text-center">
                <?= Html::img('@web/icons/alumnos.svg', ['alt' => '']) ?>
                <?= Html::a('GESTIONAR ALUMNOS', ['alumnos/index'], ['class'=>'btn btn-outline-primary']) ?>
            </div>
            
            <div class="col-lg-4 text-center">
                <?= Html::img('@web/icons/portatiles.svg', ['alt' => '']) ?>
                <?= Html::a('GESTIONAR PORTÁTILES', ['portatiles/index'], ['class'=>'btn btn-outline-primary']) ?>              
            </div>
            
            <div class="col-lg-4 text-center">
                <?= Html::img('@web/icons/almacenes.svg', ['alt' => '']) ?>
                <?= Html::a('GESTIONAR ALMACENES', ['almacenes/index'], ['class'=>'btn btn-outline-primary']) ?>
            </div>
            
        </div>

    </div>
    
</div>
