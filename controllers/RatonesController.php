<?php

namespace app\controllers;

use app\models\ratones;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * RatonesController implements the CRUD actions for ratones model.
 */
class RatonesController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all ratones models.
     *
     * @return string
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => ratones::find(),
            /*
            'pagination' => [
                'pageSize' => 50
            ],
            'sort' => [
                'defaultOrder' => [
                    'id_raton' => SORT_DESC,
                ]
            ],
            */
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single ratones model.
     * @param int $id_raton Id Raton
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id_raton)
    {
        return $this->render('view', [
            'model' => $this->findModel($id_raton),
        ]);
    }

    /**
     * Creates a new ratones model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate()
    {
        $model = new ratones();

        if ($this->request->isPost) {
            if ($model->load($this->request->post()) && $model->save()) {
                return $this->redirect(['view', 'id_raton' => $model->id_raton]);
            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing ratones model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $id_raton Id Raton
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id_raton)
    {
        $model = $this->findModel($id_raton);

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id_raton' => $model->id_raton]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing ratones model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $id_raton Id Raton
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id_raton)
    {
        $this->findModel($id_raton)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the ratones model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $id_raton Id Raton
     * @return ratones the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id_raton)
    {
        if (($model = ratones::findOne(['id_raton' => $id_raton])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
